package com.example.airtelnewux.Utils;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private int AssociatedApp;
    private String MenuName;
    private String NavigateURL;
    private String ActivityName;
    private String Image;
    private int MenuID;
    public ArrayList<ChildMenu> childMenu;

    public Menu(String s, String s1, String s2, String s3, List<ChildMenu> children) {
    }

    public ArrayList<ChildMenu> getChildMenu() {
        return childMenu;
    }

    public void setChildMenu(ArrayList<ChildMenu> childMenu) {
        this.childMenu = childMenu;
    }

    public int getAssociatedApp() {
        return AssociatedApp;
    }

    public void setAssociatedApp(int associatedApp) {
        AssociatedApp = associatedApp;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getNavigateURL() {
        return NavigateURL;
    }

    public void setNavigateURL(String navigateURL) {
        NavigateURL = navigateURL;
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String activityName) {
        ActivityName = activityName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }


    public Menu() {
    }

    public int getMenuID() {
        return MenuID;
    }

    public static class ChildMenu {
        private int AssociatedApp;
        private String MenuName;
        private String NavigateURL;
        private String ActivityName;
        private String Image;
        private int MenuID;
        public ChildMenu(String menuName, String activityName) {
            this.MenuName = menuName;
            this.ActivityName=activityName;
        }
        public ChildMenu() {

        }
        public int getAssociatedApp() {
            return AssociatedApp;
        }

        public void setAssociatedApp(int associatedApp) {
            AssociatedApp = associatedApp;
        }

        public String getMenuName() {
            return MenuName;
        }

        public void setMenuName(String menuName) {
            MenuName = menuName;
        }

        public String getNavigateURL() {
            return NavigateURL;
        }

        public void setNavigateURL(String navigateURL) {
            NavigateURL = navigateURL;
        }

        public String getActivityName() {
            return ActivityName;
        }

        public void setActivityName(String activityName) {
            ActivityName = activityName;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String image) {
            Image = image;
        }

        public int getMenuID() {
            return MenuID;
        }
    }

}