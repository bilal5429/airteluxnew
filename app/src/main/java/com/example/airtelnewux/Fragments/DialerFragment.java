package com.example.airtelnewux.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.airtelnewux.R;

public class DialerFragment extends Fragment {
    ImageView muteIcon, videoCall_Icon, addCall_Icon, notesIcon, holdCall_Icon, recordCall_Icon;
    static Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialer_fragment, container, false);
        muteIcon = view.findViewById(R.id.mute_icon_imageView);
        videoCall_Icon = view.findViewById(R.id.video_call_imageView);
        addCall_Icon = view.findViewById(R.id.add_call_imageView);
        notesIcon = view.findViewById(R.id.notes_imageView);
        holdCall_Icon = view.findViewById(R.id.hold_call_imageView);
        recordCall_Icon = view.findViewById(R.id.record_call_imageView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        muteIcon.setOnClickListener(new View.OnClickListener() {
            boolean isMuteEnable;

            @Override
            public void onClick(View view) {
                if (!isMuteEnable) {
                    muteIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isMuteEnable = true;
                } else {
                    muteIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isMuteEnable = false;
                }
            }
        });
        videoCall_Icon.setOnClickListener(new View.OnClickListener() {
            boolean isVideoCallEnable;

            @Override
            public void onClick(View view) {
                if (!isVideoCallEnable) {
                    videoCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isVideoCallEnable = true;
                } else {
                    videoCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isVideoCallEnable = false;
                }
            }
        });

        addCall_Icon.setOnClickListener(new View.OnClickListener() {
            boolean isAddCallEnable;

            @Override
            public void onClick(View view) {
                if (isAddCallEnable == false) {
                    addCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isAddCallEnable = true;

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Fragment newFragment = new DialerHeaderFragmentPersonListFragment();
                    transaction.replace(R.id.myHeader1, newFragment);
                    transaction.commit();

                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:0123456789"));
                    startActivity(intent);
                    Toast.makeText(getContext(), "Here, We will connect another caller by selecting Airtel Calling App.", Toast.LENGTH_LONG).show();

                } else {
                    addCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isAddCallEnable = false;
                }

            }
        });

        notesIcon.setOnClickListener(new View.OnClickListener() {
            boolean isNotesEnable;

            @Override
            public void onClick(View view) {
                if (isNotesEnable == false) {
                    notesIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isNotesEnable = true;
                } else {
                    notesIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isNotesEnable = false;
                }
            }
        });

        holdCall_Icon.setOnClickListener(new View.OnClickListener() {
            boolean isHoldEnable;

            @Override
            public void onClick(View view) {
                if (isHoldEnable == false) {
                    holdCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isHoldEnable = true;
                } else {
                    holdCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isHoldEnable = false;
                }
            }
        });

        recordCall_Icon.setOnClickListener(new View.OnClickListener() {
            boolean isRecordEnable;

            @Override
            public void onClick(View view) {
                if (isRecordEnable == false) {
                    recordCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isRecordEnable = true;
                } else {
                    recordCall_Icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isRecordEnable = false;
                }
            }
        });
    }
}