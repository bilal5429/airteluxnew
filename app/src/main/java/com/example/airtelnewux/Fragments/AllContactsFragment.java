package com.example.airtelnewux.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Adapters.ContactsRecyclerViewAdapter;
import com.example.airtelnewux.R;
import com.example.airtelnewux.Utils.Contact;
import com.example.airtelnewux.Utils.FastScrollRecyclerViewItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class AllContactsFragment extends Fragment {
    private RecyclerView mRecyclerView;
    public ContactsRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Cursor cursor;
    EditText searchContactEditText;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String TAG = this.getClass().getName();
    ArrayList<String> myDataset = new ArrayList<String>();
    Handler mHandlerThread;
    Thread thread1;

    public AllContactsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.all_contacts_fragment, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.my_recycler_view11);
        searchContactEditText = getActivity().findViewById(R.id.searchContactEditText);
//searchContactEditText.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View view) {
//
//    }
//});

        searchContactEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getContext().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {

            thread1 =new Thread(new Runnable() {
                @Override
                public void run() {
                    //      getFavoriteContacts();

                    Message message = new Message();
                    message.what =1;
                    message.obj = fetchContacts();
                    mHandlerThread.sendMessage(message);
                }
            });
            thread1.start();


            mHandlerThread = new Handler(){
                @Override
                public void handleMessage(@NonNull Message msg) {
                    super.handleMessage(msg);

                    myDataset = (ArrayList) msg.obj;
                    Collections.sort(myDataset);
                    HashMap<String, Integer> mapIndex = calculateIndexesForName(myDataset);

                    mAdapter = new ContactsRecyclerViewAdapter(myDataset, mapIndex);

                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getContext());
                    mRecyclerView.setLayoutManager(mLayoutManager);

                    mRecyclerView.setAdapter(mAdapter);
                    FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(getContext());
                    mRecyclerView.addItemDecoration(decoration);
                    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                }

            };
       //     myDataset =   fetchContacts();

        }
     //   myDataset = fetchContacts();

    }

    void filter(String text){
        List<String> temp = new ArrayList();
        for(String d: myDataset){

            if((d.toLowerCase().length()>text.length()?d.toLowerCase().substring(0,text.length()):"").contains(text.toLowerCase())){
                temp.add(d);
            }
        }
        //update recyclerview
        mAdapter.updateList(temp);
    }


    private HashMap<String, Integer> calculateIndexesForName(ArrayList<String> items) {
        HashMap<String, Integer> mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < items.size(); i++) {
            String name = items.get(i);
            String index = name.substring(0, 1);
            index = index.toUpperCase();

            if (!mapIndex.containsKey(index)) {
                mapIndex.put(index, i);
            }
        }
        return mapIndex;
    }

    public ArrayList fetchContacts() {

        ArrayList<String> contactsList = new ArrayList<String>();

        try {
            cursor = getActivity().getContentResolver()
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            int Idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
            cursor.moveToFirst();

            Set<String> ids = new HashSet<>();
            int count = 0;
            do {
                Log.d(TAG, "In While");
                System.out.println();
                String contactid = cursor.getString(Idx);
                if (!ids.contains(contactid)) {
                    ids.add(contactid);
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    String name = cursor.getString(nameIdx);
                    String phoneNumber = cursor.getString(phoneNumberIdx);
                    String image = cursor.getString(photoIdIdx);
                    count = count + 1;
                    Log.d(TAG, "Id--->" + contactid + "Name--->" + name + "count =" + count);
                    Log.d(TAG, "Id--->" + contactid + "Number--->" + phoneNumber);
                    Contact contact1 = new Contact();
                    contact1.setContactName(name);
                    contact1.setContactNumber(phoneNumber);
                    contactsList.add(name);
                }

            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }
        return contactsList;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                fetchContacts();
            } else {
                Toast.makeText(getContext(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
}