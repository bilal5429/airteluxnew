package com.example.airtelnewux.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Adapters.ConnectedPersonsListAdapter;
import com.example.airtelnewux.Interfaces.CallbackListener;
import com.example.airtelnewux.R;
import com.example.airtelnewux.Utils.Contact;

import java.util.ArrayList;

public class DialerHeaderFragmentPersonListFragment extends Fragment implements CallbackListener {
    RecyclerView recyclerView;
    ConnectedPersonsListAdapter adapter;
    Button mergrCallButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.conference_call_connecting_members_header, container, false);
        recyclerView = view.findViewById(R.id.connectedPersonsListRecyclerView);
        mergrCallButton = view.findViewById(R.id.merge_call_button);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Contact> contactsList = new ArrayList<>();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();
        contact1.setContactName("Bilal Khan");
        contact1.setContactNumber("+91 9718037309 ON HOLD");
        contactsList.add(contact1);

        contact2.setContactName("Chetan Aricent");
        contact2.setContactNumber("+91 9080908990 Connecting...");
        contactsList.add(contact2);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ConnectedPersonsListAdapter(getContext(), contactsList);
        adapter.setmyListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateView(boolean success) {
        mergrCallButton.setVisibility(View.VISIBLE);
        mergrCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                Fragment newFragment = new ConferenceCallHeaderFragment();

                transaction.replace(R.id.myHeader1, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}