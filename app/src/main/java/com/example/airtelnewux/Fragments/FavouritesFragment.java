package com.example.airtelnewux.Fragments;

import android.content.AsyncQueryHandler;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Activities.MyApplication;
import com.example.airtelnewux.Adapters.FavouritesRecyclerViewAdapter;
import com.example.airtelnewux.R;

import java.util.ArrayList;

public class FavouritesFragment extends Fragment {
    Handler mHandlerThread;
    Thread thread1;
    RecyclerView recyclerView;
    public FavouritesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.favourites_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        thread1 =new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"name   blblblblblblblblbl");
                Message message = new Message();
                message.what =1;
                message.obj = getFavoriteContacts();
                mHandlerThread.sendMessage(message);
            }
        });
        
   //     Runnable worker = new WorkerThread();


//        MyApplication app = new MyApplication();
//        app.getExecutor().execute(thread1);


   //     MyApplication.executor.execute(thread1);
      //  MyApplication.executor.shutdown();
            thread1.start();

          mHandlerThread = new Handler(){
              @Override
              public void handleMessage(@NonNull Message msg) {
                  super.handleMessage(msg);
                  FavouritesRecyclerViewAdapter adapter;
                    ArrayList contactsList = (ArrayList) msg.obj;
                    System.out.println(contactsList+"   blblblblblblblblbl");
                   recyclerView = getActivity().findViewById(R.id.favourites_recyclerview);
                  recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                  adapter = new FavouritesRecyclerViewAdapter(getContext(), contactsList);
                  adapter.notifyDataSetChanged();
                  recyclerView.setAdapter(adapter);

                  recyclerView.setOnTouchListener(new View.OnTouchListener() {
                      @Override
                      public boolean onTouch(View view, MotionEvent motionEvent) {
                          return false;
                      }
                  });

              }
          };





    }

    ArrayList getFavoriteContacts() {
        ArrayList contacList = new ArrayList();
        Uri queryUri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.STARRED};
        String selection = ContactsContract.Contacts.STARRED + "='1'";
        Cursor cursor = getActivity().managedQuery(queryUri, projection, selection, null, null);

        while (cursor.moveToNext()) {
            String contactID = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.Contacts._ID));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(
                    ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactID));
            intent.setData(uri);
            String intentUriString = intent.toUri(0);
            String title = (cursor.getString(
            cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
            contacList.add(title);
        }
        cursor.close();
        return contacList;
    }
}