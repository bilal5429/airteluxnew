package com.example.airtelnewux.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Activities.ContactDetailsActivity;
import com.example.airtelnewux.Activities.MainActivity2;
import com.example.airtelnewux.Adapters.RecentsRecyclerViewAdapter;
import com.example.airtelnewux.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class RecentsFragment extends Fragment {
    private BottomSheetBehavior bottomSheet = null;

    public RecentsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cricket, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bottomSheet = BottomSheetBehavior.from(getActivity().findViewById(R.id.bs));
        ImageView calling_button = getActivity().findViewById(R.id.calling_button);
        calling_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), MainActivity2.class);
                startActivity(i);
                getActivity().finish();
            }
        });
        bottomSheet.setHideable(true);
        bottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        final FloatingActionButton fab = getActivity().findViewById(R.id.fab_Recents);
        fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                fab.hide();
            }
        });
        bottomSheet.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    fab.show();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        RecentsRecyclerViewAdapter adapter;
        ArrayList contactsList = new ArrayList();
        contactsList.add(1);
        contactsList.add(2);
        contactsList.add(3);
        contactsList.add(4);
        contactsList.add(5);
        contactsList.add(6);
        contactsList.add(7);
        contactsList.add(8);
        contactsList.add(9);
        contactsList.add(10);
        contactsList.add(11);
        contactsList.add(12);
        contactsList.add(13);
        contactsList.add(14);
        contactsList.add(15);
        contactsList.add(16);
        contactsList.add(17);

        RecyclerView recyclerView = getActivity().findViewById(R.id.recents_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecentsRecyclerViewAdapter(getContext(), contactsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
            }
        });
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
//                System.out.println(bottomSheet.getState() + "   bilal");
//                if (bottomSheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//                    bottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
//                }
                return false;
            }
        });

        recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                System.out.println(bottomSheet.getState() + "   bilal");
                if (bottomSheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
          //      return false;


            }
        });
    }
}
