package com.example.airtelnewux.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.airtelnewux.R;

import java.util.Timer;
import java.util.TimerTask;

public class DialerHeaderFragment extends Fragment {
    Handler handler;
    TextView timerTextView;
    int minCount = 0;
    int hourCount = 0;
    TextView callEnded_textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialer_header_fragment, container, false);

        timerTextView = view.findViewById(R.id.timer_textView);
        callEnded_textView = view.findViewById(R.id.callEnded_textView);
        return view;
    }

    @SuppressLint("HandlerLeak")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int delay = 1000; // delay for 5 sec.
        int period = 1000; // repeat every sec.

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            int count = 0;
            public void run() {
                count++;
                Message msg = new Message();
                msg.what = 1;
                msg.obj = count;
                handler.sendMessage(msg);
            }
        }, delay, period);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {

                if (msg.what == 1) {
                    int count = (int) msg.obj;
                    count = count % 60;

                    if (count == 0) {
                        minCount++;
                    }
                    if (minCount == 60) {
                        minCount = 0;
                        hourCount++;
                    }

                    if (count <= 9 && minCount <= 9) {
                        timerTextView.setText("0" + hourCount + ":0" + minCount + ":" + "0" + count);
                    } else if (count > 9 && minCount <= 9) {
                        timerTextView.setText("0" + hourCount + ":0" + minCount + ":" + count);
                    } else if (count > 9 && minCount > 9) {
                        timerTextView.setText("0" + hourCount + ":" + minCount + ":" + count);
                    } else if (count <= 9 && minCount > 9) {
                        timerTextView.setText("0" + hourCount + ":" + minCount + ":0" + count);
                    }
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callEnded_textView.setVisibility(View.VISIBLE);

    }
}