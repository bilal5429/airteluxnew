package com.example.airtelnewux.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.airtelnewux.Adapters.MyAdapter;
import com.example.airtelnewux.R;
import com.google.android.material.tabs.TabLayout;

public class DialerTabbedActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ImageView settingsImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_dialer_tabbed);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        settingsImageView = findViewById(R.id.settings_imageView);
        tabLayout.addTab(tabLayout.newTab().setText("Favourites"));
        tabLayout.addTab(tabLayout.newTab().setText("Recents"));
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        settingsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(DialerTabbedActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        });
        tabLayout.getTabAt(0).setCustomView(R.layout.favourites_custom_tab);
        tabLayout.getTabAt(1).setCustomView(R.layout.recents_custom_tab);
        tabLayout.getTabAt(2).setCustomView(R.layout.contacts_custom_tab);
        final MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    TextView textView = findViewById(R.id.favourites_custom_tab);
                    textView.setTextColor(Color.BLUE);
                    findViewById(R.id.volte_connected_text).setVisibility(View.GONE);
                } else if (tab.getPosition() == 1) {
                    TextView textView = findViewById(R.id.recents_custom_tab);
                    textView.setTextColor(Color.BLUE);
                    findViewById(R.id.volte_connected_text).setVisibility(View.VISIBLE);
                } else if (tab.getPosition() == 2) {
                    TextView textView = findViewById(R.id.contacts_custom_tab);
                    textView.setTextColor(Color.BLUE);
                    findViewById(R.id.volte_connected_text).setVisibility(View.GONE);
                }
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    TextView textView = findViewById(R.id.favourites_custom_tab);
                    textView.setTextColor(Color.GRAY);
                } else if (tab.getPosition() == 1) {
                    TextView textView = findViewById(R.id.recents_custom_tab);
                    textView.setTextColor(Color.GRAY);
                } else if (tab.getPosition() == 2) {
                    TextView textView = findViewById(R.id.contacts_custom_tab);
                    textView.setTextColor(Color.GRAY);
                }
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}