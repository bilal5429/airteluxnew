package com.example.airtelnewux.Activities;

import android.app.Application;
import android.os.StrictMode;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyApplication extends Application {
public  ExecutorService executor;
    @Override
    public void onCreate() {

        System.out.println("hello in applicationnnn");
         executor = Executors.newFixedThreadPool(5);//creating a pool of 5 threads
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
        super.onCreate();
    }

    public ExecutorService getExecutor(){
        return executor;
    }
}
