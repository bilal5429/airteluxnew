package com.example.airtelnewux.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.airtelnewux.Fragments.DialerHeaderFragment;
import com.example.airtelnewux.R;


public class MainActivity2 extends AppCompatActivity {
    ImageView speakerIcon, dialerPadIcon;
    TextView timerTextView;
    ImageView call_hangup_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main2);
        speakerIcon = findViewById(R.id.speaker_imageView);
        dialerPadIcon = findViewById(R.id.dialerpad_imageView);
        timerTextView = findViewById(R.id.timer_textView);
        call_hangup_button = findViewById(R.id.call_hangup_button);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment newFragment = new DialerHeaderFragment();
        transaction.replace(R.id.myHeader1, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();


        speakerIcon.setOnClickListener(new View.OnClickListener() {
            boolean isSpeakerEnabled;

            @Override
            public void onClick(View view) {
                if (!isSpeakerEnabled) {
                    speakerIcon.setColorFilter(ContextCompat.getColor(MainActivity2.this,
                            R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isSpeakerEnabled = true;
                } else {
                    speakerIcon.setColorFilter(ContextCompat.getColor(MainActivity2.this,
                            R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isSpeakerEnabled = false;
                }
            }
        });
        call_hangup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        dialerPadIcon.setOnClickListener(new View.OnClickListener() {
            boolean isDialerPadEnabled;

            @Override
            public void onClick(View view) {
                if (!isDialerPadEnabled) {
                    dialerPadIcon.setColorFilter(ContextCompat.getColor(MainActivity2.this,
                            R.color.colorButtonClicked), android.graphics.PorterDuff.Mode.SRC_IN);
                    isDialerPadEnabled = true;
                } else {
                    dialerPadIcon.setColorFilter(ContextCompat.getColor(MainActivity2.this,
                            R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                    isDialerPadEnabled = false;
                }
            }
        });
    }
}