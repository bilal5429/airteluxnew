package com.example.airtelnewux.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.example.airtelnewux.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
       getSupportActionBar().setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SimpleExpandableListAdapter expListAdapter =
                new SimpleExpandableListAdapter(
                        this,
                        createGroupList(),
                        R.layout.parent,
                        new String[] { "Group Item" },
                        new int[] { R.id.row_name },
                        createChildList(),
                        R.layout.child,
                        new String[] {"Sub Item"},
                        new int[] { R.id.grp_child}
                );
        ExpandableListView expandableListView =findViewById(R.id.ExpandableListView);
        expandableListView.setAdapter( expListAdapter );
}

    @SuppressWarnings("unchecked")
    private List createGroupList() {
        ArrayList result = new ArrayList();
        for( int i = 0 ; i < 4 ; ++i ) {
            HashMap m = new HashMap();
            m.put( "Group Item","Group Item " + i );
            result.add( m );
        }
        return (List)result;
    }
    @SuppressWarnings("unchecked")
    private List createChildList() {
        ArrayList result = new ArrayList();
        for( int i = 0 ; i < 15 ; ++i ) {
            ArrayList secList = new ArrayList();
            for( int n = 0 ; n < 1 ; n++ ) {
                HashMap child = new HashMap();
                child.put( "Sub Item", "Sub Item " + n );
                secList.add( child );
            }
            result.add( secList );
        }
        return result;
    }
    public void  onContentChanged  () {
        Log.d(TAG, "onContentChanged");
        super.onContentChanged();
    }

    public boolean onChildClick( ExpandableListView parent, View v, int groupPosition,int childPosition,long id) {
        Log.d(TAG, "Inside onChildClick at groupPosition = " + groupPosition +" Child clicked at position " + childPosition);
        return true;
    }

    public void  onGroupExpand  (int groupPosition) {
        try{
            Log.d(TAG, "Group expanding Listener => groupPosition = " + groupPosition);
        }catch(Exception e){
            Log.d(TAG, " groupPosition Errrr +++ " + e.getMessage());
        }
    }
}