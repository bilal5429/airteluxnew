package com.example.airtelnewux.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Utils.Contact;
import com.example.airtelnewux.Adapters.ManageCallersAdapter;
import com.example.airtelnewux.R;

import java.util.ArrayList;

public class ManageCallersActivity extends AppCompatActivity implements ManageCallersAdapter.ItemClickListener {
    Button proceedButton;
    ManageCallersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_callers);

        RecyclerView recyclerView = findViewById(R.id.manageCallersRecyclerView);
        proceedButton = findViewById(R.id.proceed_button);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        ArrayList<Contact> contactsList = new ArrayList<>();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();

        contact1.setContactName("Bilal Khan");
        contact1.setContactNumber("+91 9718037309 ON HOLD");
        contactsList.add(contact1);

        contact2.setContactName("Chetan Aricent");
        contact2.setContactNumber("+91 9080908990 Connecting...");
        contactsList.add(contact2);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ManageCallersAdapter(this, contactsList);
        recyclerView.setAdapter(adapter);
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}