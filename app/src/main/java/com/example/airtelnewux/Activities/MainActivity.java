package com.example.airtelnewux.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.airtelnewux.R;

public class MainActivity extends AppCompatActivity {

    Thread thread1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        thread1= new Thread(new Runnable() {
            @Override
            public void run() {
                getSupportActionBar().hide();
            }
        });

        setContentView(R.layout.activity_main);
        LinearLayout splashScreen = findViewById(R.id.splashScreen);

        splashScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
                finish();
            }
        });
    }
}