package com.example.airtelnewux.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import com.example.airtelnewux.Adapters.FavouritesRecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.view.MotionEvent;
import android.view.View;

import com.example.airtelnewux.R;

import java.util.ArrayList;

public class ContactDetailsActivity extends AppCompatActivity {
    Handler mHandlerThread;
    Thread thread1;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        thread1 =new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"name   blblblblblblblblbl");
                Message message = new Message();
                message.what =1;
                message.obj = getFavoriteContacts();
                mHandlerThread.sendMessage(message);
            }
        });

  //      MyApplication.executor.execute(thread1);
        //  MyApplication.executor.shutdown();
        //    thread1.start();

        mHandlerThread = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                FavouritesRecyclerViewAdapter adapter;
                ArrayList contactsList = (ArrayList) msg.obj;
                System.out.println(contactsList+"   blblblblblblblblbl");
                recyclerView =findViewById(R.id.favourites_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(ContactDetailsActivity.this));
                adapter = new FavouritesRecyclerViewAdapter(ContactDetailsActivity.this, contactsList);
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);

                recyclerView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        return false;
                    }
                });

            }
        };



        ArrayList contactsList = getFavoriteContacts();
        FavouritesRecyclerViewAdapter adapter;
        RecyclerView recyclerView = findViewById(R.id.contact_details_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FavouritesRecyclerViewAdapter(this, contactsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });
    }

    ArrayList getFavoriteContacts() {
        ArrayList contacList = new ArrayList();
        Uri queryUri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.STARRED};
        String selection = ContactsContract.Contacts.STARRED + "='1'";
        Cursor cursor = this.managedQuery(queryUri, projection, selection, null, null);

        while (cursor.moveToNext()) {
            String contactID = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.Contacts._ID));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(
                    ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactID));
            intent.setData(uri);
            String intentUriString = intent.toUri(0);
            String title = (cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
            contacList.add(title);
        }
        cursor.close();
        return contacList;
    }

}