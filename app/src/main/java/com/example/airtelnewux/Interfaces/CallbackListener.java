package com.example.airtelnewux.Interfaces;

public interface CallbackListener {
    public void updateView(boolean success);
}