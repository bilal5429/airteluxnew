package com.example.airtelnewux.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Interfaces.FastScrollRecyclerViewInterface;
import com.example.airtelnewux.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by flaviusmester on 23/02/15.
 */
public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> implements FastScrollRecyclerViewInterface {
    private ArrayList<String> mDataset;
    private HashMap<String, Integer> mMapIndex;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(LinearLayout v) {
            super(v);
            mTextView = v.findViewById(R.id.contactNameTextView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ContactsRecyclerViewAdapter(ArrayList<String> myDataset, HashMap<String, Integer> mapIndex) {
        mDataset = myDataset;
        mMapIndex = mapIndex;
    }
    public void updateList(List<String> list){
        mDataset = (ArrayList<String>) list;
        notifyDataSetChanged();
    }
    // Create new views (invoked by the layout manager)
    @Override
    public ContactsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
//        ...
        ViewHolder vh = new ViewHolder((LinearLayout)v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public HashMap<String, Integer> getMapIndex() {
        return this.mMapIndex;
    }
}