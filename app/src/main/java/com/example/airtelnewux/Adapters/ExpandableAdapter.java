//package com.example.airtelnewux;
//
//import android.content.Context;
//import android.graphics.BitmapFactory;
//import android.util.Base64;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseExpandableListAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//public class ExpandableAdapter extends BaseExpandableListAdapter {
//    private final Context context;
//    private final List<Menu> parentObjects;
//
//    public ExpandableAdapter(Context context, ArrayList<Menu> parentObjects) {
//        this.context = context;
//        this.parentObjects = parentObjects;
//
//    }
//
//    @Override
//    public int getGroupCount() {
//        return parentObjects.size();
//    }
//
//    @Override
//    public int getChildrenCount(int i) {
//        return parentObjects.get(i).childMenu.size();
//    }
//
//    @Override
//    public Menu getGroup(int i) {
//        return parentObjects.get(i);
//    }
//
//    @Override
//    public Menu.ChildMenu getChild(int i, int i2) {
//        return parentObjects.get(i).childMenu.get(i2);
//    }
//
//    @Override
//    public long getGroupId(int i) {
//        return i;
//    }
//
//    @Override
//    public long getChildId(int i, int i2) {
//        return 0;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
//        Menu currentParent = parentObjects.get(i);
//        if (view == null) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
//                    .LAYOUT_INFLATER_SERVICE);
//            view = inflater.inflate(R.layout.navdrawer_parent_item, viewGroup,false);
//        }
//
//        ImageView imageViewIndicator = (ImageView) view.findViewById(R.id.imageViewNav);
//        if (getChildrenCount(i) == 0)
//            imageViewIndicator.setVisibility(View.GONE);
//        else
//            imageViewIndicator.setVisibility(View.VISIBLE);
//
//        TextView textViewNavMenuName = (TextView) view.findViewById(R.id.textViewNavParentMenuName);
//        ImageView imageViewIcon = (ImageView) view.findViewById(R.id.imageViewIcon);
//        String base64 = currentParent.getImage();
//        if (base64 != null && !base64.equals("")) {
//            byte[] imageAsBytes = Base64.decode(currentParent.getImage().getBytes(), Base64
//                    .DEFAULT);
//            imageViewIcon.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0,
//                    imageAsBytes.length));
//
//        } else
//            imageViewIcon.setImageResource(R.drawable.ic_action_android);
//
//        textViewNavMenuName.setText(currentParent.getMenuName());
//        return view;
//    }
//
//
//
//    @Override
//    public View getChildView(int groupPosition, int childPosition, boolean b, View view,
//                             ViewGroup viewGroup) {
//        Menu currentChild = getGroup(groupPosition);
//        if (view == null) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
//                    .LAYOUT_INFLATER_SERVICE);
//            view = inflater.inflate(R.layout.navdrawer_child_item, viewGroup,false);
//        }
//        View divider = view.findViewById(R.id.divider);
//        if (b)
//            divider.setVisibility(View.VISIBLE);
//        else
//            divider.setVisibility(View.GONE);
//
//        TextView textViewNavMenuName = (TextView) view.findViewById(R.id.textViewNavChildMenuName);
//        textViewNavMenuName.setText(currentChild.childMenu.get(childPosition).getMenuName());
//
//        return view;
//    }
//
//    @Override
//    public boolean isChildSelectable(int i, int i2) {
//        return true;
//    }
//}
