package com.example.airtelnewux.Adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Interfaces.CallbackListener;
import com.example.airtelnewux.Utils.Contact;
import com.example.airtelnewux.R;

import java.util.List;

public class ConnectedPersonsListAdapter extends RecyclerView.Adapter<ConnectedPersonsListAdapter.ViewHolder> {

    private List<Contact> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    TextView contactNameTextView, contactNumberTextView;
    Context context;
    int minCount = 0;
    int hourCount = 0;


    // data is passed into the constructor
    public ConnectedPersonsListAdapter(Context context, List<Contact> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.connected_person_row, parent, false);
        return new ViewHolder(view);

    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact data = mData.get(position);

        contactNameTextView.setText(data.getContactName());
        contactNumberTextView.setText(data.getContactNumber());

        if(position == 0) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    contactNumberTextView.setText("+91 9080908990 Connected");
                    listener.updateView(true);

                }
            }, 5000);


        }

}

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ViewHolder(View itemView) {
            super(itemView);
            contactNameTextView = itemView.findViewById(R.id.contactNameTextView);
            contactNumberTextView = itemView.findViewById(R.id.contactNumberTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Contact getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    CallbackListener listener;

    public void setmyListener (CallbackListener listener){
        this.listener = listener;
    }

}