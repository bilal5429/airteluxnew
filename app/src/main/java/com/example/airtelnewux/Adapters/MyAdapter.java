package com.example.airtelnewux.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.airtelnewux.Fragments.AllContactsFragment;
import com.example.airtelnewux.Fragments.FavouritesFragment;
import com.example.airtelnewux.Fragments.RecentsFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MyAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public MyAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FavouritesFragment footballFragment = new FavouritesFragment();
                return footballFragment;
            case 1:
                RecentsFragment cricketFragment = new RecentsFragment();
                return cricketFragment;
            case 2:

                AllContactsFragment nbaFragment = new AllContactsFragment();
                return nbaFragment;

            case 3:
                AllContactsFragment nbaFragment1 = new AllContactsFragment();
                return nbaFragment1;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }


    private HashMap<String, Integer> calculateIndexesForName(ArrayList<String> items){
        HashMap<String, Integer> mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i<items.size(); i++){
            String name = items.get(i);
            String index = name.substring(0,1);
            index = index.toUpperCase();

            if (!mapIndex.containsKey(index)) {
                mapIndex.put(index, i);
            }
        }
        return mapIndex;
    }
}

