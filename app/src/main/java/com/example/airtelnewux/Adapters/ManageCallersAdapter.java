package com.example.airtelnewux.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.Utils.Contact;
import com.example.airtelnewux.R;

import java.util.List;


public class ManageCallersAdapter extends RecyclerView.Adapter<ManageCallersAdapter.ViewHolder> {

    private List<Contact> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    TextView contactNameTextView, contactNumberTextView;
    Button removeButton;
    Context context;


    // data is passed into the constructor
    public ManageCallersAdapter(Context context, List<Contact> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.manage_callers_row, parent, false);
        return new ViewHolder(view,this);
    }

    public void removeItem(int position) {
        mData.remove(position);
        Toast.makeText(context,"Caller removed",Toast.LENGTH_LONG).show();
        notifyItemRemoved(position);
        // Add whatever you want to do when removing an Item
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact data = mData.get(position);

        contactNameTextView.setText(data.getContactName());
        contactNumberTextView.setText(data.getContactNumber());

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ManageCallersAdapter myAdapter;
        ViewHolder(View itemView,ManageCallersAdapter adapter) {
            super(itemView);
            myAdapter = adapter;
            contactNameTextView = itemView.findViewById(R.id.contactNameTextView);
            contactNumberTextView = itemView.findViewById(R.id.contactNumberTextView);
            removeButton = itemView.findViewById(R.id.removeButton);
            removeButton.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(view, getAdapterPosition());

            if(view.getId() == removeButton.getId())
            myAdapter.removeItem(getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Contact getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}