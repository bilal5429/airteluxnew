package com.example.airtelnewux.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.airtelnewux.R;

import java.util.List;

public class RecentsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    int type1 = 1;
    int type2 = 2;
    View view =null;
    RecyclerView.ViewHolder viewHolder = null;
    Context context;
    Spinner spin;

    // data is passed into the constructor
    public RecentsRecyclerViewAdapter(Context context, List data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==type1)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_all_calls_layout,parent,false);
            viewHolder = new ViewHolderOne(view);
        }
        else
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recents_row,parent,false);
            viewHolder= new ViewHolderTwo(view);
        }


        //View view = mInflater.inflate(R.layout.recents_row, parent, false);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int data = (int) mData.get(position);
        spin = (Spinner)view.findViewById(R.id.all_calls_spinner);
if(position == 0){
    String[] country = { "All Calls", "Outgoing", "Incoming", "Missed"};

//        spin.setOnItemSelectedListener(getActivity());

    //Creating the ArrayAdapter instance having the country list
    ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,country);
    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    //Setting the ArrayAdapter data on the Spinner
    if(spin!= null)
    spin.setAdapter(aa);
}

     //   holder.contactNameTextView.setText(data+"");
    }

    // binds the data to the TextView in each row
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
////        holder.contactNumberTextView.setText(data.getContactNumber());
//
//    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView contactNameTextView, contactNumberTextView;

        ViewHolder(View itemView) {
            super(itemView);
            contactNameTextView = itemView.findViewById(R.id.contactNameTextView);
            contactNumberTextView = itemView.findViewById(R.id.contactNumberTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return type1;
        }
        else
            return type2;

    }

    //****************  VIEW HOLDER 1 ******************//

    public class ViewHolderOne extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolderOne(View itemView) {
            super(itemView);
         //   name = (TextView)itemView.findViewById(R.id.displayName);
        }
    }


    //****************  VIEW HOLDER 2 ******************//

    public class ViewHolderTwo extends RecyclerView.ViewHolder{

        public ViewHolderTwo(View itemView) {
            super(itemView);

        }
    }

    // convenience method for getting data at click position
//    Contact getItem(int id) {
//        return mData.get(id);
//    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}